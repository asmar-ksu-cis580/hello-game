﻿using ColorMine.ColorSpaces;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace HelloGame
{
    public class HelloGame : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private Vector2 puckPosition;
        private Vector2 puckVelocity;
        private Texture2D puckTexture;

        private Hsl backgroundColor;

        public HelloGame()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            Window.Title = "Hello Game";
        }

        protected override void Initialize()
        {
            var bounds = GraphicsDevice.Viewport.Bounds;
            puckPosition = bounds.Center.ToVector2();

            var random = new System.Random();
            puckVelocity = new Vector2((float)random.NextDouble(), (float)random.NextDouble());
            puckVelocity.Normalize();
            puckVelocity *= 100;

            backgroundColor = new Hsl
            {
                H = 0.4,
                S = 0.37,
                L = 0.37,
            };

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            puckTexture = Content.Load<Texture2D>("puck");
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            var bounds = GraphicsDevice.Viewport.Bounds;

            puckPosition += (float)gameTime.ElapsedGameTime.TotalSeconds * puckVelocity;
            if (puckPosition.X <= bounds.Left || puckPosition.X >= bounds.Right - puckTexture.Width)
            {
                puckVelocity.X *= -1;
            }
            if (puckPosition.Y <= bounds.Top || puckPosition.Y >= bounds.Bottom - puckTexture.Height)
            {
                puckVelocity.Y *= -1;
            }

            backgroundColor.H += gameTime.ElapsedGameTime.TotalSeconds * 100;
            backgroundColor.H %= 360d;

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            var backgroundRgb = backgroundColor.To<Rgb>();
            GraphicsDevice.Clear((new Color((int)backgroundRgb.R, (int)backgroundRgb.G, (int)backgroundRgb.B)));

            _spriteBatch.Begin();
            _spriteBatch.Draw(puckTexture, puckPosition, Color.White);
            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}